// Fetch API, export fuctions
const axios = require('axios')

function loadObjects (name, hair, year) {
  var swapiData = {}
  swapiData.name = name
  swapiData.hair = hair
  swapiData.BYear = year
  console.log('Name: ' + swapiData.name)
  console.log('Hair: ' + swapiData.hair)
  console.log('DOB: ' + swapiData.BYear)
}

const swapiDt = {
  get: (id) => {
    var url = 'https://swapi.co/api/people/' + id + '/'
    axios.get(url)
      .then((response) => {
        loadObjects(response.data.name, response.data.hair_color, response.data.birth_year)
      })
      .catch((error) => {
        console.log(error)
      })
      .finally(() => {
        console.log('run!')
      })
  },
  pass: () => {
    console.log('pass')
  }
}

export default swapiDt
