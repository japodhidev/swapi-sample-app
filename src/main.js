import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
// import swapiDt from './api.js'

Vue.config.productionTip = false

// const getS = swapiDt.get(1)
// Vue Instance
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
